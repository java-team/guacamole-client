#!/bin/sh
#
# Simple postinst script for Guacamole which creates a "guacamole-web" group
# and sets the permissions of user-mapping.xml (a file which must sometimes
# contain plaintext passwords) appropriately.
#

# Exit on errors
set -e

GUAC_GROUP="guacamole-web"
GUAC_FILES="/etc/guacamole/user-mapping.xml"
GUAC_FILE_OWNERSHIP="root:$GUAC_GROUP"
GUAC_FILE_MOD="640"

# Convenience function for error conditions
fail() {
    echo "$1" >&2
    exit 1
}

#DEBHELPER#

# Create guacamole-web group if it does not exist
groupadd -fr "$GUAC_GROUP" ||\
    fail "Could not create group \"$GUAC_GROUP\""

# Change ownership and permissions of Guacamole files
for FILE in $GUAC_FILES
do

    # Update ownership
    chown "$GUAC_FILE_OWNERSHIP" "$FILE" ||\
        fail "Unable to change ownership of $FILE"

    # Update permissions
    chmod "$GUAC_FILE_MOD" "$FILE" ||\
        fail "Unable to change permissions of $FILE"

done

